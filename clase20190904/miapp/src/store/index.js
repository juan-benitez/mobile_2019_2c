import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default function () {
  const Store = new Vuex.Store({
    state: {
      apellido: '',
      nombre: '',
      usuario: '',
      rol: 'aca va el rol',
      contador: 0
    },
    mutations: {
      guardarUsuario: (state, apellido, nombre, usuario) => {
        console.log('Ejecutando mutacion guardarUsuario')

        state.apellido = apellido
        state.nombre = nombre
        state.usuario = usuario
      },
      incrementar: function (state) {
        state.contador++
      },
      decrementar: function (state) {
        state.contador--
      },
      cambiarNombre: function (state, nombreCompleto) {
        let na = nombreCompleto.split(' ')
        state.apellido = na[0]
        state.nombre = na[1]
      },
      cambiarRol: function (state, rol) {
        state.rol = rol
      }
    },
    getters: {
      getContador: function (state) {
        return state.contador
      },
      getNombreCompleto: function (state) {
        return state.apellido + ' ' + state.nombre
      }
    }
  })

  return Store
}
