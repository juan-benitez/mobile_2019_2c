import React, { Component } from 'react';
import { Text, View, Button, Alert } from 'react-native';

export default class HelloWorld extends Component {

    buttonPressed () {
        Alert.alert("Hello !!");
    }

    render() {
        const cuerpo = <Text>Hello World react native</Text>

        return (
            <View>
                {cuerpo}
                <Button
                    title="Push me"
                    onPress={this.buttonPressed}
                />
            </View>
        );
    }
}