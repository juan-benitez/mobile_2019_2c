import React, { Component } from 'react';
import { Text, View, StyleSheet, FlatList } from 'react-native';
import lista from '../data/Todos';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22
    }
})

class Item extends Component {
    render() {
        return (
            <View>
                <Text>{this.props.todo.Titulo}</Text>
                <Text>{this.props.todo.Detalle}</Text>
            </View>
        );
    }
}

export default class TodoList extends Component {

    

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={lista}
                    renderItem={({ item }) => <Item todo={item}/>}
                    keyExtractor={item => item.id}
                />
            </View>
        );
    }
}