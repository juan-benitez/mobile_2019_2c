import React, { Component } from 'react';
import { Text, Container, Content, List, ListItem, Header } from 'native-base';
import getTodos from '../service/Todos';
import { getPlaneDetection } from 'expo/build/AR';

export default class TodoList extends Component {

    render() {
        return (
            <Container>
                <Header/>
                <Content>
                    <Text>Lista de TODO v2</Text>
                    <List>
                        <ListItem>
                            <Text>Uno</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Dos</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Tres</Text>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        );
    }
}