const fetch = require('node-fetch')

console.log('Hola NodeJS')

fetch('https://swapi.co/api/people')
    .then((resp) => {
        console.log('llego la respuesta')
        console.log(resp)
        if (resp.ok)
            resp.json()
                .then((datos) => {
                    console.log(datos.count)
                    console.log(datos.results[0].name)

                    datos.results.forEach(element => {
                        console.log(element.name)
                    });
                })

    })
    .catch(error => {
        console.log('Se rompio:')
        console.log(error);
    })

