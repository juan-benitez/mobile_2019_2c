import Vue from 'vue'
import Vuex from 'vuex'

import productos from './products'

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    state: {
      productos,
      carro: []
    },
    mutations: {
      agregarEnCarrito: function(state, producto) {
        state.carro.push(producto)
      }
    }
  })

  return Store
}
