# TP 1. VueJS-Quasar

## Descripción de la App

Realizar una aplicación para hacer match entre diversas personas interesadas en jugar en un determinado rango horario.

## Estructura de las entidades:

### User
- username
- password
- email
- birthdate
- nickname
- avatar

### Game
- id
- name
- tags []
- image
- esrb

### window_time_frame
- day_time_start
- day_time_end

### User_preferences
- game_id
- window_time_frames [ { window_time_frame } ]
- minimal_played_hours
- user_played_hours

### Login response
- access_token
- user:
    - username
    - email
    - birthdate
    - nickname
    - avatar

### Register response
- success (boolean)

### CRUD Game
- GET /game/
    - [{ game }]
- GET /game/:id
    - { game }

