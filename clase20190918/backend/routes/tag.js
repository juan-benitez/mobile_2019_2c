const express = require('express');
const router = express.Router();
const models = require('../models');

router.get('/', (req, res) => {
    models.Tag.findAll()
        .then((rows) => {
            res.send(rows);
        });
});

router.get('/:id', (req, res) => {
    let id = req.params.id;

    models.Tag.findOne({
        where: {
            id: id
        }
    })
        .then((row) => {
            res.send(row);
        });
});

router.post('/', (req, res) => {
    let tagname = req.body.tagname;

    models.Tag.create({
        tagname,
    }).then((user) => {
        res.sendStatus(201);
    })
});

router.put('/:id', (req, res) => {
    let tagname = req.body.tagname;

    models.Tag.update(
        {
            tagname
        },
        {
            where: { id }
        }
    ).then(() => {
        res.sendStatus(200);
    })
});

router.delete('/:id', (req, res) => {
    let id = req.params.id;

    models.Tag.destroy({
        where: {
            id
        }
    }).then(() => {
        res.sendStatus(200);
    })
});

module.exports = router;