const express = require('express');
const router = express.Router();
const models = require('../models');

router.get('/', (req, res) => {
    models.User.findAll()
        .then((rows) => {
            res.send(rows);
        });
});

router.get('/:id', (req, res) => {
    let id = req.params.id;

    models.User.findOne({
        where: {
            id: id
        }
    })
        .then((row) => {
            res.send(row);
        });
});

router.post('/', (req, res) => {
    let username = req.body.nombre;
    let password = req.body.apellido;
    let email = req.body.edad;
    let birthdate = req.body.departamentoid;
    let nickname = req.body.nickname;
    let avatar = req.body.avatar;

    models.User.create({
        username,
        password,
        email,
        birthdate,
        nickname,
        avatar
    }).then((user) => {
        res.sendStatus(201);
    })
});

router.put('/:id', (req, res) => {
    let username = req.body.nombre;
    let password = req.body.apellido;
    let email = req.body.edad;
    let birthdate = req.body.departamentoid;
    let nickname = req.body.nickname;
    let avatar = req.body.avatar;

    models.User.update(
        {
            username,
            password,
            email,
            birthdate,
            nickname,
            avatar
        },
        {
            where: { id }
        }
    ).then(() => {
        res.sendStatus(200);
    })
});

router.delete('/:id', (req, res) => {
    let id = req.params.id;

    models.User.destroy({
        where: {
            id
        }
    }).then(() => {
        res.sendStatus(200);
    })
});

module.exports = router;