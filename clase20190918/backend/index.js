const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const routerUser = require('./routes/user');
const routerTag = require('./routes/tag');
const routerGame = require('./routes/game');

const security = require('./security');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.post('/login', security.login)

app.use('/user', security.checkToken, routerUser);
app.use('/tag', security.checkToken, routerTag);
app.use('/game', security.checkToken, routerGame);

app.listen(port=5000, () => {
    console.log('Server listen...');
});