module.exports = (sequelize, Sequelize) => {

    let TimeFrame = sequelize.define('TimeFrame', {
        day: Sequelize.INTEGER,
        from_time: Sequelize.INTEGER,
        to_time: Sequelize.INTEGER
    });

    return TimeFrame;
};