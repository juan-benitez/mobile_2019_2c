module.exports = (sequelize, Sequelize) => {

    let UserPreference = sequelize.define('UserPreference', {
        minimal_played_hours: Sequelize.INTEGER,
        user_played_hours: Sequelize.INTEGER
    });

    return UserPreference;
};