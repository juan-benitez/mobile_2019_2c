const Sequelize = require('sequelize');
const User = require('./user');
const Tag = require('./tag');
const Game = require('./game');
const TimeFrame = require('./timeframe');
const UserPreference = require('./userpreference');

let db = {};

const sequelize = new Sequelize('gameDB', null, null, {
    dialect: "sqlite",
    storage: "./gameDB.sqlite"
});

sequelize.authenticate()
    .then(function () { console.log('Autenticado'); })
    .catch(function (err) { console.log('Error autenticando: ' + err); })

// Entities
db.User = User(sequelize, Sequelize);
db.Tag = Tag(sequelize, Sequelize);
db.Game = Game(sequelize, Sequelize);
db.TimeFrame = TimeFrame(sequelize, Sequelize);
db.UserPreference = UserPreference(sequelize, Sequelize);

// Relations
db.UserPreference.hasMany(db.TimeFrame);
db.User.hasMany(db.UserPreference);
db.Game.belongsToMany(db.Tag, { through: 'GamesTags' });
db.Tag.belongsToMany(db.Game, { through: 'GamesTags' });

db.sync = function () {
    sequelize.sync({ force: true })
        .then(function (err) {
            console.log('Model syncronized.');
        });
}

// db.sync();

module.exports = db;