module.exports = (sequelize, Sequelize) => {

    let User = sequelize.define('User', {
        username: Sequelize.STRING,
        password: Sequelize.STRING,
        email: Sequelize.INTEGER,
        birthdate: Sequelize.DATE,
        nickname: Sequelize.STRING,
        avatar: Sequelize.BLOB
    });

    return User;
};