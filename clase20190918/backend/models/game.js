module.exports = (sequelize, Sequelize) => {

    let Game = sequelize.define('Game', {
        name: Sequelize.STRING,
        image: Sequelize.BLOB('long'),
        esrb: Sequelize.STRING
    });

    return Game;
};