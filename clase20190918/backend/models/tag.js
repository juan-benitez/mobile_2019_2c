module.exports = (sequelize, Sequelize) => {

    let Tag = sequelize.define('Tag', {
        tagname: Sequelize.STRING,
    });

    return Tag;
};