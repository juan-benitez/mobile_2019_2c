import Router from 'vue-router';
import Cliente from '../components/Cliente';

export default new Router({
    routes: [{
        path: '/cliente',
        component: Cliente
    }]
});