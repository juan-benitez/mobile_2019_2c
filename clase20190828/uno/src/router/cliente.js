import Router from 'vue-router'
import AltaCliente from '../components/AltaCliente'
import BajaCliente from '../components/BajaCliente'
import ModificarCliente from '../components/ModificarCliente'

export default new Router({
    routes: [{
        path: '/cliente/alta',
        component: AltaCliente
    }, {
        path: '/cliente/baja',
        component: BajaCliente
    }, {
        path: '/cliente/modificar',
        component: ModificarCliente
    }]
});